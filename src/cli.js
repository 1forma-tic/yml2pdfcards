#!/usr/bin/env node
const main = require("./main.cjs");
const converter = main.default;
const fs = require("fs");

if(process.argv.length < 4) usageError('not enough arguments');
if(process.argv.length > 4) usageError('too many arguments');

let srcFilePath = process.argv[2];
let targetPath = process.argv[3];

if(fs.lstatSync(srcFilePath).isFile()) processOneFile(srcFilePath,targetPath);
else if(fs.lstatSync(srcFilePath).isDirectory()){
  const srcPath = trimLastSlash(srcFilePath);

  fs.readdirSync(srcPath).forEach((srcFile)=>processOneFile(`${srcPath}/${srcFile}`,targetPath));
} else usageError(`${srcFilePath} is not found as file or directory.`);

function processOneFile(src,targetPath){
  const ext = src.split('.').pop();
  if(ext === "yml" || ext === "yaml") {
    targetPath = trimLastSlash(targetPath);
    const originalFileName = src.split('/').pop().split('.').shift();
    const outputFilesStrings = converter(fs.readFileSync(src,"utf8"),originalFileName);
    Object.keys(outputFilesStrings).forEach((fileName)=>{
      const pathParts = [...targetPath.split('/'), ...fileName.split('/')] ;
      pathParts.pop();
      if(pathParts.length>0) fs.mkdirSync(pathParts.join('/'),{recursive: true});
      fs.writeFileSync(`${targetPath}/${fileName}`, outputFilesStrings[fileName])
    });
  }
}
function usageError(customMessage){
  const programName = process.argv[1].split('/').pop();
  if(customMessage) console.error(`\nError : ${customMessage}`);
  console.error(`\nUsage : ${programName} srcFileOrFolder targetFolder`);
  process.exit(1);
}
function trimLastSlash(path){
  if(path[path.length-1] === '/') path = path.substr(0,path.length-1);
  return path;
}

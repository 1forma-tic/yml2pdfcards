import latinize from '../node_modules/latinize-to-ascii/latinize.mjs'
import yaml from '../node_modules/js-yaml/dist/js-yaml.mjs'

function normalizedString(str) {
	return latinize(str).replace(/[^A-Za-z0-9]/g, '-').replace(/-{2,}/g, '-');
}

function converter(ymlContent, ymlFileName=''){
  const jsonData = yaml.load(ymlContent);
	if(!jsonData.fileName) jsonData.fileName = ymlFileName || "Result";
  console.log("filename:",jsonData.fileName);
  jsonData.fileName = normalizedString(jsonData.fileName);
  const svg = JSON.stringify(jsonData);// convert2svg(jsonData);
  const pdf = svg2pdf(svg);
  return {
    [`${jsonData.fileName}.svg`]: svg,
    [`${jsonData.fileName}.pdf`]: pdf,
	};
}
export default converter
function svgCard(card){
	return `<div class="page">
${card}
</div>`;
}
function svgCards(cards){
  return cards.map(svgCard).join('\n');
}
function svgPage(page){
  return page.map(svgCards).join('\n');
}
function convert2svg(obj) {
  return obj;
	return `<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
${svgPage(obj)}
</body>
</html>
	`;
}
function svg2pdf(svg) {
  return JSON.stringify(svg);
}

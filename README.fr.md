[EN](README.md) | FR

# Yml2pdfCards

Convertie un fichier yaml en svg puis en fichier pdf A4 avec des cartes prêtes à imprimer dedans.

## [Convertisseur en ligne](https://1forma-tic.frama.io/yml2pdfcards/)

## Exemples
- [pdf généré](https://1forma-tic.frama.io/yml2pdfcards/demo-examples/2x5cards.pdf) et le [fichier source yml](https://framagit.org/1forma-tic/yml2pdfcards/raw/main/demo-examples/2x5cards.yml) dont il est issu.

## Usage en ligne de commande :
```
npx yml2pdfcards srcFileOrFolder targetFolder
```

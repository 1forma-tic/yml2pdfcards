EN | [FR](README.fr.md)

# Yml2pdfCards

Convert yaml file to svg then A4 pdf with ready to print cards in it.

## [Online converter](https://1forma-tic.frama.io/yml2pdfcards/)

## Examples
- [generated pdf](https://1forma-tic.frama.io/yml2pdfcards/demo-examples/2x5cards.pdf) and [yml source](https://framagit.org/1forma-tic/yml2pdfcards/raw/main/demo-examples/2x5cards.yml) to build it.

## Command line usage
```
npx yml2pdfcards srcFileOrFolder targetFolder
```
